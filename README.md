# Sign Language App 👐👋🤘

## Tech Stack used :- 
  - Python
  - Dart / Flutter  

## Inspiration💡
It was very difficult for the Deaf community or people with hearing loss to communicate or learn alphabets through the sign language they know or for sounded people to know what sign language hand expression means which letter. So we built this user friendly moblie application to eradict and make a bridge for the deaf community and us to communicate.


## What is the project about 🔨
Do you want to learn how to communicate with the Deaf community or people with hearing loss who may use ***American Sign Language (ASL)*** ? Or interested in learning a new language? Learning sign language is easier than ever, thanks to the internet. Just like so many other things, there’s an app for that. There are numerous apps to learn American Sign Language (ASL) outside the old classroom method.


## What it does 🧭
This app helps users learn sign language. It includes 24 alphabets signs and features a predictive search engine, so it’s easier to find what you need. It will be a great place to learn the basics. This app is best for learning the ASL alphabet 

## This is how it looks / Some peep into the project 💫
![WhatsApp Image 2022-05-15 at 11 02 17 PM](https://user-images.githubusercontent.com/75165587/168606552-c68029ce-a74d-4e09-91a6-cb8a6a9d9513.jpeg)

<!-- App icon 👁️
<img src="https://user-images.githubusercontent.com/75165587/149612182-8a76fbef-dfa5-46a5-b8e8-1e3f189997a3.jpeg" width="100"> -->

## The App 🤜🔥🤛
![Caasdpture](https://user-images.githubusercontent.com/75165587/168606683-f4791c54-0610-491b-b444-934ef813a7dc.PNG)


## Collaborators 🤖

| Name      | GitHub Profile     |   |
| :------------- | :----------: | :----------: |
|  Chetas Shree Madhusudhan   | [GitHub](https://github.com/ChetasShree) |  Tech Associate / Flutter |
|  Aluru Leela   | [GitHub](https://github.com/LEELARANIALURU) |  AI/ML |
|  Aryaman Shrivastava   | [GitHub](https://github.com/aryamanshrivastava) | Flutter |
|  Routhu Manoj   | [GitHub](https://github.com/Manoj-Routhu) |  AI/ML |

